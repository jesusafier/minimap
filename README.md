<h1>MiniMap</h1>

A simple minimap module for foundryVTT.

Currently has the following features:

1. Shows a minimap with a controllable view port, pan the map using the minimap.
2. Players view shows only explored areas.
3. All player token are shown live on the minimap.

![](https://i.imgur.com/4feonG4.mp4)

<h1>Install</h1>
<h2>On foundryVTT modules list</h2>
Search for MiniMap


<h2>Manual Install</h2>
https://gitlab.com/jesusafier/minimap/-/jobs/artifacts/master/raw/module.json?job=build-module

Join the discussions on our [Discord](https://discord.gg/467HAfZ)
and if you liked it, consider supporting me on [patreon](https://www.patreon.com/foundry_grape_juice)